require("chromedriver");
const webdriver = require("selenium-webdriver");
const chrome = require("selenium-webdriver/chrome");
let chrome_options = new chrome.Options();
const driver = new webdriver.Builder().forBrowser("chrome").setChromeOptions(chrome_options).build();

const email = "info@risetribe.online";
const pass = "Risetribe";
const loginURL = `https://rise-wellness.au1.cliniko.com/users/sign_in?email=${email.replace("@", "%40")}`;

let clientName = "";
let paymentSource = "";

async function loginToCliniko() {
    // navigate to page
    await driver.get(loginURL);

    // enter password
    await driver.findElement(webdriver.By.css("#password")).sendKeys(pass);

    // stay logged in
    await driver
        .findElement(
            webdriver.By.css(
                "#cliniko-angular > div > div > div > form.login-form.ng-pristine.ng-valid > div > div.md-flex.flex-justify.flex-center > label > div"
            )
        )
        .click();

    // click login
    await driver
        .findElement(
            webdriver.By.css("#cliniko-angular > div > div > div > form.login-form.ng-pristine.ng-valid > div > div.md-flex.flex-justify.flex-center > button")
        )
        .click();
}

async function searchClientsInvoices(client) {
    // click clients
    let button = await driver.wait(
        webdriver.until.elementLocated(
            webdriver.By.css(
                "#app-header > div.app-header__nav-container.gm-autoshow.gm-scrollbar-container > div.gm-scroll-view > nav > main-navigation-links > a:nth-child(3)"
            )
        ),
        10000
    );
    await button.click();

    let searchBar = await driver.wait(webdriver.until.elementLocated(webdriver.By.css("#filter-table")), 10000);
    await searchBar.sendKeys(client);

    let clientLink = await driver.wait(
        webdriver.until.elementLocated(
            webdriver.By.css(
                "#cliniko-angular > div > main > ui-view > ui-view > patients-index > page > section > div > page-content > div.ng-scope > div > table > tbody > tr:nth-child(2) > th > a"
            )
        ),
        10000
    );
    await clientLink.click();

    let invoices = driver.wait(webdriver.until.elementLocated(webdriver.By.css("#patient-navigation a[href*='invoices']")), 10000);
    await invoices.click();
}

async function getLastInvoice() {
    await driver.wait(webdriver.until.elementsLocated(webdriver.By.css("span[ng-if*='isOpen']")), 10000);

    await driver.executeScript(`
        let invoices = document.querySelectorAll("span[ng-if*='isOpen']");
        invoices[invoices.length - 1].previousElementSibling.click();
    `);
}

async function enterPayment(paymentSourceSelected) {
    let enterPaymentBtn = await driver.wait(
        webdriver.until.elementLocated(
            webdriver.By.css(
                "#cliniko-angular > div > main > div > div > section > div.flex-grow.page__content > div.flex.flex-justify.below-md-flex-column > div > div > a"
            )
        ),
        10000
    );

    await enterPaymentBtn.click();

    // next page

    let paymentAmount = parseFloat(await driver.wait(webdriver.until.elementLocated(webdriver.By.css("span[data-js-payment-outstanding]"))).getText());
    console.log("Payment amount: " + paymentAmount);

    // find correct input by label
    paymentSourceSelected = paymentSourceSelected.toLowerCase();
    await driver.executeScript(`
        document.querySelectorAll("label[class='rf-label'][for*='payment']").forEach((elem) => {
            const name = elem.textContent.toLowerCase();
            if (name === "${paymentSourceSelected}") {
                elem.setAttribute(name, "");
            }
        });
    `);

    // get correct input
    const paymentAmountInput = await driver.findElement(
        webdriver.By.css(`label[class='rf-label'][for*='payment'][${paymentSourceSelected}] + div > input[type='number']`)
    );
    // send payment amount to the input
    await paymentAmountInput.sendKeys(paymentAmount);

    // scroll
    await driver.executeScript("window.scrollTo(0, document.body.scrollHeight)");

    // clicking on the first (of many) invoices fills them all out
    await driver.findElement(webdriver.By.css("#payment_payment_allocations_attributes_0_amount")).click();

    // save payment
    await driver.findElement(webdriver.By.css("#new_payment > div > button")).click();
}

async function promptForNameAndPayment() {
    async function isAlertPresent() {
        try {
            await driver.switchTo().alert();
            return true;
        } catch (e) {
            return false;
        }
    }

    await driver.executeScript(`
        let name = prompt("Continue? Enter Name (q for quit):");
        let paymentSource = "";
        if (name === 'q') {
            document.body.setAttribute('Name', 'q');
        } 
        else if (name === null) {
            document.body.setAttribute('Name', 'p');
        }
        else {
            paymentSource = prompt("Enter Payment Source (IVAC, CSST, SAAQ) (q for quit):");
            if (paymentSource === 'q') {
                document.body.setAttribute('Name', 'q');
                document.body.setAttribute('paymentSource', 'q');
            }
            else if (paymentSource === null) {
                document.body.setAttribute('paymentSource', 'p');
            }
        }

        document.body.setAttribute('Name', name);
        document.body.setAttribute('paymentSource', paymentSource);
    `);
    while (await isAlertPresent()) {
        await driver.sleep(1000);
    }
    const body = await driver.findElement(webdriver.By.css("body"));
    clientName = await body.getAttribute("name");
    paymentSource = await body.getAttribute("paymentSource");

    if (clientName === "q" || paymentSource === "q") {
        console.log("Quitting...");
        driver.quit();
    } else if (clientName === "p" || paymentSource === "p") {
        // ask for input again in 1 min
        setTimeout(promptForNameAndPayment, 1000 * 60);
    }

    console.log(clientName, paymentSource);
}

async function main() {
    await driver.manage().window().maximize();

    // if (process.argv.length !== 4) return;

    // clientName = process.argv[2];
    // paymentSource = process.argv[3];

    await promptForNameAndPayment();
    console.log(clientName, paymentSource);

    await loginToCliniko();
    while (true) {
        await searchClientsInvoices(clientName);
        await getLastInvoice();
        await enterPayment(paymentSource);
        await driver.get("https://rise-wellness.au1.cliniko.com/");
        await promptForNameAndPayment();
    }
}

main();
